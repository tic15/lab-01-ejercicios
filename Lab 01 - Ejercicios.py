import math;
import re;

### LAB 02

## 1

## 1.1
a = int(input("Valor de a: "));
b = int(input("Valor de b: "));
c = int(input("Valor de c: "));

disc = (b**2) - (4 * a * c);

if (disc < 0):
    print("error");
else: 
    print("Resultado:", (-b + math.sqrt(disc)) / 2 * a);

## 1.2
x = int(input("Valor de x: "));
y = int(input("Valor de y: "));
u = int(input("Valor de u: "));
w = int(input("Valor de w: "));

num = x + y;
denom = u + (w / a);

print("Resultado:", num / denom);

## 1.3
z = int(input("Valor de z: "));
print("Resultado:", (x / y) * (z + w));

## 1.4
print("Resultado:", 5 * (x + y));



## 2 Una persona requiere cambiar cierta cantidad de dólares a soles, realizar un programa que le facilite la labor (TC: 1$=3.8 Soles).
dollars = int(input("Dolares: "));
print(f"Equivale a s/{dollars * 3.8}");



## 3 Se requiere obtener la distancia entre dos puntos en el plano cartesiano, tal y como se muestra en la figura.

x1 = int(input("Valor de X1: "));
x2 = int(input("Valor de X2: "));
y1 = int(input("Valor de Y1: "));
y2 = int(input("Valor de Y2: "));

d = math.sqrt(((x2 - x1)**2) + ((y2 - y1)**2));

print(f"La distancia entre los puntos es: {d}");



## 4 Tiendas Metro posee una campaña de descuento del 20% del total a pagar a las personas mayores de 30 años y que superen los S/.1200.00 soles del valor de compra. Se requiere un programa que verifique las personas favorecidas.

age = int(input("Edad: "));
amount = int(input("Total compra: "));

if (age > 30 and amount > 1200):
    print(f"TOTAL: {amount - (amount * 0.30)}");
else:
    print("Sin descuento.");



## 5 Realizar un programa que permita el ingreso por teclado de 2 números, y los intercambie de valor, se debe mostrar los originales y cambiados.

numA = int(input("Primer numero: "));
numB = int(input("Segundo numero: "));

aux = numA;
numA_ = numB;
numB_ = aux;

print(f"INGESO:     numA={numA} y numB={numB}");
print(f"RESULTADO:     numA={numA_} y numB={numB_}");



## 6 Ingresar 3 números por teclado y mostrar lo siguiente: 
## - El mayor de ellos
## - El promedio del menor y mayor.
## - Mostrar ordenado de forma ascendente.
## - No se debe permitir el ingreso de cero ni negativos.

nums = [
    int(input("Primer numero: ")),
    int(input("Segundo numero: ")),
    int(input("Tercero numero: "))
];
nums.sort();

# print(f"El numero mayor es: {max(*nums)}");
# print(f"El numero menor es: {min(*nums)}");

mayor = nums[0];
menor = nums[0];

for i in range(3):
    actual_num = nums[i];
    if (actual_num > mayor):
        mayor = actual_num;
    elif (actual_num < menor):
        menor = actual_num;

print(f"El numero mayor es: {mayor}");
print(f"El numero menor es: {menor}");
print(f"El promedio del número mayor y menor es: {(mayor + menor) / 2}");
print(f"Orden ascendente: {nums}");



##7 Escribir un programa que pregunte a un empleado por el número de horas trabajadas y el coste por hora. Después debe mostrar por pantalla el salario que le corresponde.

hours = int(input("Numero de horas trabajadas: "));
monto_por_hora = int(input("Costo por hora: "));

print(f"Salario: s/{hours * monto_por_hora}");



##8 Escribir un programa que pida al usuario su peso (en kg) y estatura (en metros), calcule el índice de masa corporal y lo almacene en una variable, y muestre por pantalla la frase: Tu índice de masa corporal es (IMC) calculado y debe ser redondeado con dos decimales.

weight = int(input("Peso(kg): "));
height = float(input("Estatura(m): "));

imc = weight / (height**2);

print(f"Masa corporal: {round(imc, 2)}");



## 9 Escribir un programa que pregunte al usuario una cantidad a invertir, el interés anual y el número de años, y muestre por pantalla el capital obtenido en la inversión.

initial_capital = int(input("Cantidad a invertir: "));
annual_interest = int(input("Interes anual: "));
years = int(input("Años: "));

final_capital = initial_capital + (annual_interest * years);

print(f"El capital final es: {final_capital}");



## 10 Una panadería vende unidades de pan a S/0.30 cada una. El pan que no es el día tiene un descuento del 60% Escribir un programa que comience leyendo el número de unidades vendidas que no son del día. Después el programa debe mostrar el precio habitual de una unidad de pan, el descuento que se le hace por no ser fresca y el coste final total.

bread_cost = 0.30;
amount_expired_bread_sold = int(input("Ingresa el número de panes vendidos que no son del día: "));

print(f"Precio del pan: s/{bread_cost}");
print(f"60% de descuento por no ser pan del dia! -(s/{bread_cost * 0.60} c/u)");
print(f"Total a pagar: s/{round((amount_expired_bread_sold * bread_cost) * 0.60, 2)}");



## 11 Escribir una función que tome un carácter y devuelva True si es una vocal, de lo contrario devuelve False.

def isVowel(vowel):
    match = re.match("^[aeiou]$", vowel);
    print(True if match else False);

isVowel("a");



## 12 Escribir un programa que pregunte el nombre del usuario en la consola y un número entero e imprima por pantalla en líneas distintas el nombre del usuario tantas veces como el número introducido. 

username = input("Nombre de usuario: ");
times = int(input("Ingresa un número: "));

for i in range(times):
    print(username);
